cmake_minimum_required(VERSION 3.16)
project(Student_Record)

option(DEBUG "Use this option to turn on debug mode" OFF)

if(DEBUG)
    set(CMAKE_BUILD_TYPE Debug)
    set(CMAKE_CXX_FLAGS -g)
else()
    set(CMAKE_BUILD_TYPE Release)
    set(CMAKE_CXX_FLAGS -O3)
endif()
set(CMAKE_CXX_FLAGS -Wall)

include_directories(include)
add_subdirectory(app)
add_subdirectory(src)