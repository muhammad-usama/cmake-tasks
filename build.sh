# !/bin/bash
# Check build directory. If it does not exist, then create a build directory.
if [ ! -d "$(pwd)/build" ] ; then
    mkdir build
fi
# Change directory
cd build
# If -d flag is provided by the user, then build the project in debug mode. 
# DThe default build type is Release.
if [ $# -eq 1 ]; then
    if [ $1 = "-d" ]; then
        cmake -DDEBUG="ON" ..
    fi
else
    cmake ..
fi
cmake --build .
