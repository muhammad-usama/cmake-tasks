#include <string>
#include <map>

class student 
{
    private:
        struct student_record 
        {
            std::string roll_no;
            int age;
            float cgpa;
        }student_info;
        std::map<std::string /*subject name*/, int /*marks*/> result;

    public:
        student();
        student(std::string roll_no, int age, float cgpa);
        ~student();
        int get_subject_marks(std::string subject);
        void set_subject_marks(std::string subject, int marks);
        void print_all_marks();

        // Setter Functions
        void set_rollno(std::string roll_no);
        void set_age(int age);
        void set_cgpa(float cgpa);
        // Getter Functions
        std::string get_rollno();
        int get_age();
        float get_cgpa();      
};

