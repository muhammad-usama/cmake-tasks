# Cmake Tasks

## Getting started
- In your Linux terminal, go to your working directory and write **git clone https://gitlab.com/muhammad-usama/cmake-tasks.git** to clone this repository.
- Now change your directory using **cd cmake-tasks**
- To build this project use
  - For debug mode: **bash build.sh -d**
  - For release mode: **bash build.sh**
- To run the executable file use **./build/app/Student_Record**
