#include <iostream>
#include "student.h"

using namespace std;


student::student()
{
  //  cout << "Default constructor is called." << endl;
    student_info.roll_no = "NULL";
    student_info.age = 5;
    student_info.cgpa = 0.0;
}

student::student(string roll_no = "NULL", int age = 0, float cgpa = 0.0)
{
  //  cout << "Parameterized constructor is called." << endl;
    set_rollno(roll_no);
    set_age(age);
    set_cgpa(cgpa);
}


int student:: get_subject_marks(string subject)
{
    if (result.find(subject) != result.end())
        return result[subject];
    else
    {
        cout << "The subject '" << subject << "' doesn't exist in the record!" << endl;
        return 0;
    }
}

void student::set_subject_marks(string subject, int marks)
{
    while(true)
    {
        if(marks >= 0)
        {
            result[subject] = marks;
            return;
        }
        else
        {
            cout << "You have entered invalid marks.\nEnter marks greater than or equal to 0.";
            cin >> marks;
        }
    }
 
}

void student::print_all_marks()
{
    map<string,int> :: iterator iter;
    for(iter = result.begin(); iter != result.end(); iter++)
    {
        cout << (*iter).first << "\t" << (*iter).second << endl;
    }
}

void student::set_age(int age)
{
    while(true)
    {
        if(age >= 5)
        {
            student_info.age = age ;
            break;
        }
        else
        {
            cout << "Invalid age.\nEnter age greater than or equal to 5:";
            cin >> age;
        }
    }
}

void student::set_cgpa(float cgpa)
{
    while(true)
    {
        if(cgpa >= 0 && cgpa <= 4)
        {
            student_info.cgpa = cgpa ;
            break;
        }
        else
        {
            cout << "Invalid cgpa.\nEnter cgpa between 0 - 4:";
            cin >> cgpa;
        }
    }
}

void student::set_rollno(std::string roll_no)
{
    student_info.roll_no = roll_no;
}

string student::get_rollno()
{
    return student_info.roll_no;
}

int student::get_age()
{
    return student_info.age;
}

float student::get_cgpa()
{
    return student_info.cgpa;
}   

student::~student()
{
  // cout << "Destructor is called." << endl;
}