#include <iostream>
#include <vector>
#include "student.h"

using namespace std;

#define MAX_SIZE 5

void Display_Menu()
{
    cout << "Press 1 to add new student." << endl;
    cout << "Press 2 to add subject marks." << endl;
    cout << "Press 3 to search student." << endl;
    cout << "Press 4 to display students data." << endl;
    cout << "Press 5 to display all roll no." << endl;
    cout << "Press any other key to exit." << endl;
    cout << "Option: " ;
}

int Search_Student(student student_record[],const int size, string roll_no)
{
    for(int index = 0; index < size; index++)
    {
        if(student_record[index].get_rollno() == roll_no)
            return index;
    }
    return -1;
}

void Add_Student(student student_record[],int &size)
{
    if(size == MAX_SIZE)
    {
        cout << "Array is full." << endl;
        return;
    }
    else
    {
        string roll_no;
        int age;
        float cgpa;
        cout << "\n-----------------------------------------------------\n";
        while(true)
        {
            cout << "Enter roll number: " ;
            cin >> roll_no;
            if(Search_Student(student_record,size,roll_no) >= 0)
                cout << "Roll number already exist." << endl;
            else
                break;
        }
        cout << "Enter age: ";
        cin >> age;
        cout << "Enter cgpa: ";
        cin >> cgpa; 

        student_record[size].set_rollno(roll_no);
        student_record[size].set_age(age);
        student_record[size].set_cgpa(cgpa);
        size++;
        cout << "successfully added student data in the record." << endl;
        cout << "\n-----------------------------------------------------\n";
    }
}

void Display_Student_Data(student student_record[], const int size)
{
    string roll_no;
    cout << "\n-----------------------------------------------------\n";
    cout << "Enter roll number: ";
    cin >> roll_no;
    int index = Search_Student(student_record,size,roll_no);
    if(index >= 0)
    {
        cout << "Roll No: " << student_record[index].get_rollno() << endl;
        cout << "CGPA: " << student_record[index].get_cgpa() << endl; 
        cout << "Age: " << student_record[index].get_age() << endl;
        student_record[index].print_all_marks();
    }
    else
        cout << "Student data is not present in the record.." << endl;
    cout << "\n-----------------------------------------------------\n";


}

void Set_Student_Marks(student student_record[], const int size)
{
    string roll_no;
    cout << "\n-----------------------------------------------------\n";
    cout << "Enter roll number: ";
    cin >> roll_no;
    int index = Search_Student(student_record,size,roll_no);
    if(index >= 0)
    {
        int sub_count, marks;
        string subject;
        cout << "How many subject marks you want to enter? " ;
        cin >> sub_count;
        for(int index = 0; index < sub_count; index++)
        {
            cout << "Enter Subject: " ;
            cin >> subject;
            cout << "Enter Marks: ";
            cin >> marks;
            student_record[index].set_subject_marks(subject,marks);
        }
    }
    else
        cout << "Student data is not present in the record.." << endl;
    cout << "\n-----------------------------------------------------\n";
}
void Display_all_students_rollno(student student_data[], const int size)
{
    cout << "\n------------------------------------------------\n";
    for(int index = 0; index < size; index++)
    {
        cout << index+1 << ". " << student_data[index].get_rollno() << endl;
    }
    cout << "\n------------------------------------------------\n";
}
int main()
{
    char option;
    int size = 0;
    string roll_no;
    student student_record[MAX_SIZE];
    do
    {
        Display_Menu();
        cin >> option;
        switch(option)
        {
            case '1':
                Add_Student(student_record,size);
                break;
            case '2':
                Set_Student_Marks(student_record,size);
                break;
            case '3':
                cout << "Enter roll number: ";
                cin >> roll_no;
                if(Search_Student(student_record,size,roll_no) >= 0)
                    cout << "Student data is present in the record." << endl;
                else
                    cout << "Student data is not present in the record.." << endl;
                break;
            case '4':
                Display_Student_Data(student_record, size);
                break;
            case '5':
                Display_all_students_rollno(student_record,size);
                break;
            default:
                option = 'E';
                break;
        }
    } while (option != 'E');
    
    
    return 0;
}